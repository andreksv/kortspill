package edu.ntnu.idatt2001.cardgame;

import java.util.List;
import java.util.ArrayList;
import edu.ntnu.idatt2001.cardgame.GUI.*;
import java.util.Random;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;


import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

        @DisplayName("Test getDeck and constructor")
        @Nested
        class testGetDeck{
            DeckOfCards deck;

            @DisplayName("Make a deck object")
            @BeforeEach
            public void makeADeck() {
                deck = new DeckOfCards();
            }

            @DisplayName("Check if deck has 52 PlayingCard")
            @Test
            public void checkIfDeckHas52Cards(){
                assertEquals(52, deck.getDeck().size());
            }

            @DisplayName("Check if deck has duplicates")
            @Test
            public void checkIfDeckHasDuplicates(){
                List<PlayingCard> dupeDeck = deck.getDeck();
                assertTrue(deck.getDeck().stream().anyMatch(dupeDeck::contains));
            }
        }

        @DisplayName("test dealHand")
        @Nested
        class testDealHand {

            DeckOfCards deck;
            ArrayList<PlayingCard> hand;

            @BeforeEach
            public void makeADeck() {
                deck = new DeckOfCards();
            }

            @DisplayName("Check if possible to make a hand of more than 52 cards")
            @Test
            public void makeAHandOfMoreThan52Cards() {
                try {
                    hand = deck.dealHand(100);
                    fail("Method did not throw IndexOutOfBoundsException as expected");
                } catch (IndexOutOfBoundsException ex) {
                    assertEquals(ex.getMessage(), "You can´t have a hand of less than 1 or more than 52 cards.");
                }
            }

            @DisplayName("Check if possible to make a hand of less than one card")
            @Test
            public void makeAHandOfLessThanOneCard() {
                try {
                    hand = deck.dealHand(-25);
                    fail("Method did not throw IndexOutOfBoundsException as expected");
                } catch (IndexOutOfBoundsException ex) {
                    assertEquals(ex.getMessage(), "You can´t have a hand of less than 1 or more than 52 cards.");
                }
            }

            @DisplayName("Check if a full hand has duplicates")
            @Test
            public void checkAFullHandHasDuplicates() {
                ArrayList<PlayingCard> compareHand = new ArrayList<>();
                hand = deck.dealHand(52);
                hand.stream().filter(card -> !(compareHand.add(card))).collect(Collectors.toList()); //A PlayingCard is only added to compareHand if its not already in compareHand.
                assertEquals(hand, compareHand);
                assertTrue(hand.stream().anyMatch(hand::contains));
            }

            @DisplayName("Check if a deck of a possible random size has duplicates")
            @Test
            public void checkIfADeckOfARandomAmountHasDuplicates() {
                Random randomNumber = new Random();
                ArrayList<PlayingCard> compareHand = new ArrayList<>();
                hand = deck.dealHand(randomNumber.nextInt(0, 52));
                hand.stream().filter(card -> !compareHand.add(card)).collect(Collectors.toList()); //A PlayingCard is only added to compareHand if its not already in compareHand.
                assertEquals(hand, compareHand);
            }
        }
}
