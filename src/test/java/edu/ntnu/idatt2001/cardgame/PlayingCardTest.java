package edu.ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
public class PlayingCardTest {

    @DisplayName("Test getMethods, toString and equals")
    @Nested
    class getMethods{
        PlayingCard card;
        @BeforeEach
        public void makePlayingCard(){
            card = new PlayingCard('H', 5);
        }

        @DisplayName("Test getSuit")
        @Test
        public void getSuitTest(){
            assertEquals(card.getSuit(),'H');
        }

        @DisplayName("Test getFace")
        @Test
        public void getFaceTest(){
            assertEquals(card.getFace(),5);
        }

        @DisplayName("Test toString")
        @Test
        public void toStringTest(){
            assertEquals(card.toString(), "H5");
        }
        @DisplayName("Test equals")
        @Test
        public void equalsTest(){
            assertTrue(card.equals(new PlayingCard('H',5)));
        }
    }

}
