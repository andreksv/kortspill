package edu.ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {

    @DisplayName("Test getHand")
    @Nested
    class getHand{
        HandOfCards hand;
        DeckOfCards deck = new DeckOfCards();

        @Test
        public void getHand(){
            ArrayList<PlayingCard> compareHand = deck.dealHand(52);
            hand = new HandOfCards(compareHand);
            assertEquals(hand.getHand(), compareHand);
        }
    }

    @DisplayName("Test getSumOfFaces and getCardsOfHearts")
    @Nested
    class getSumOfFacesAndGetHearts{
        HandOfCards hand;
        DeckOfCards deck = new DeckOfCards();
        @BeforeEach
        public void makeHand(){
            hand = new HandOfCards(deck.dealHand(52));
        }

        @DisplayName("Test getSumOfFaces method")
        @Test
        public void getSumOfFaces(){
            Integer sumFaces = hand.getHand().stream().mapToInt(PlayingCard::getFace).sum();
            assertEquals(sumFaces, hand.getSumOfTheFaces());
        }

        @DisplayName("Test getCardsOfHearts method")
        @Test
        public void getCardsOfHeartWithEveryHeart(){
            ArrayList<PlayingCard> handOfHearts = hand.getCardsOfHearts();
            int index = 1;
            for(PlayingCard card : handOfHearts){
                assertTrue(handOfHearts.contains(new PlayingCard('H',index)));
                index+=1;
            }
        }

        @DisplayName("Check getCardsOfHearts when there are no hearts")
        @Test
        public void getCardsOfHeartWithNoHeartCards(){
            HandOfCards testHand = new HandOfCards(new ArrayList<PlayingCard>());
            assertTrue(testHand.getCardsOfHearts().isEmpty());
        }
    }


    @DisplayName("Test hasFlush and hasQueenOfSpades")
    @Nested
    class checkIfFlushAndQueenOfSpades {
        HandOfCards hand;
        DeckOfCards deck = new DeckOfCards();

        @DisplayName("Test hasFlush when you have a flush")
        @Test
        public void hasFlush(){
            ArrayList<PlayingCard> fiveHearts = new ArrayList<>();
            fiveHearts.add(new PlayingCard('H',1));
            fiveHearts.add(new PlayingCard('H',2));
            fiveHearts.add(new PlayingCard('H',3));
            fiveHearts.add(new PlayingCard('H',4));
            fiveHearts.add(new PlayingCard('H',5));
            hand = new HandOfCards(fiveHearts);
            assertTrue(hand.hasFlush());
        }

        @DisplayName("Test hasFlush when you DONT have a flush")
        @Test
        public void doesntHaveFlush(){
            ArrayList<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('S',11));
            cards.add(new PlayingCard('H',2));
            cards.add(new PlayingCard('C',3));
            cards.add(new PlayingCard('H',13));
            cards.add(new PlayingCard('D',1));
            cards.add(new PlayingCard('S',12));
            cards.add(new PlayingCard('H',7));
            hand = new HandOfCards(cards);
            assertFalse(hand.hasFlush());
        }

        @DisplayName("Test hasQueenOfSpade when you have queen of spade")
        @Test
        public void hasQueenOfSpade(){
            ArrayList<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('S',12)); //<- Queen of spade
            cards.add(new PlayingCard('H',2));
            cards.add(new PlayingCard('C',3));
            hand = new HandOfCards(cards);
            assertTrue(hand.hasQueenOfSpade());
        }

        @DisplayName("Test hasQueenOfSpade when you DONT have queen of spade")
        @Test
        public void doesntHaveQueenOfSpade(){
            ArrayList<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('H',12));
            cards.add(new PlayingCard('D',12));
            cards.add(new PlayingCard('C',12));
            hand = new HandOfCards(cards);
            assertFalse(hand.hasQueenOfSpade());
        }
    }
}
