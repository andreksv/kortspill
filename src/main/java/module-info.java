module edu.ntnu.idatt2001.kortspill {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.cardgame to javafx.fxml;
    exports edu.ntnu.idatt2001.cardgame;
    exports edu.ntnu.idatt2001.cardgame.GUI;
    opens edu.ntnu.idatt2001.cardgame.GUI to javafx.fxml;
}