package edu.ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * HandOfCards represent a hand of PlayingCard´s.
 * Uses the method dealHand from class DeckOfCards.
 * to make a hand.
 */
public class HandOfCards {
    private ArrayList<PlayingCard> hand;

    /**
     * The class constructor.
     * @param hand   The hand of PlayingCard´s you want to use.
     */
    public HandOfCards(ArrayList<PlayingCard> hand){
        this.hand = hand;
    }

    public ArrayList<PlayingCard> getHand(){
        return hand;
    }

    /**
     * Gets the sum of all the face values of the cards.
     * @return   Sum of all the face values
     */
    public int getSumOfTheFaces(){
        return hand.stream().map(PlayingCard::getFace).mapToInt(Integer::intValue).sum();
    }
    /**
     * Get a string of all the Heart cards in the hand.
     * @return   All the heart cards or the String "No Hearts" if there are no heart cards.
     */
    public ArrayList<PlayingCard> getCardsOfHearts(){
        return (ArrayList<PlayingCard>) hand.stream().filter(card -> card.getSuit() == 'H').collect(Collectors.toList());
    }

    /**
     * Checks if the hand as the card "S12" (Queen of spades).
     * @return   True or False depending on if it has it.
     */
    public boolean hasQueenOfSpade(){
        return hand.stream().anyMatch(card -> card.getFace() == 12 && card.getSuit() =='S');
    }

    /**
     * Checks if the hand has a flush:
     * Five cards of the same suit (can be either
     * hearts, diamonds, spades or clubs)
     * @return   True or False depending on if you have a flush.
     */
    public boolean hasFlush(){
        Map<Character, List<PlayingCard>> fiveFlush = hand.stream().collect(groupingBy((PlayingCard::getSuit)));
        return fiveFlush.keySet().stream().anyMatch(suit -> fiveFlush.get(suit).size() >= 5 );
    }
}
