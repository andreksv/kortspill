package edu.ntnu.idatt2001.cardgame.GUI;

import edu.ntnu.idatt2001.cardgame.DeckOfCards;
import edu.ntnu.idatt2001.cardgame.HandOfCards;
import edu.ntnu.idatt2001.cardgame.PlayingCard;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class CardGameGUI extends Application{
    /**
     * Main GUI display of the application.
     */
    DeckOfCards cards = new DeckOfCards();
    HandOfCards hand;
    @Override
    public void start(Stage stage) {
        stage.setTitle("CardGame");
        StackPane stackPane = new StackPane();
        HBox amountCardsLayout = new HBox();
        VBox buttonLayout = new VBox(15);
        VBox attributeLayout = new VBox(50);

        // Text for check hand button
        Text sumOfFacesText = new Text("Sum of the faces: ");
        Text cardsofHeartsText = new Text("Cards of hearts: ");
        Text flushText = new Text("Flush: ");
        Text queenOfSpadesText = new Text("Queen of spades: ");

        // TextField for entering deck size
        TextField amountCards = new TextField();
        Text textAmountCards = new Text("Enter deck size -> ");
        textAmountCards.setFont(Font.font("", FontWeight.BLACK, 18));
        amountCardsLayout.getChildren().addAll(textAmountCards, amountCards);
        amountCardsLayout.setMaxWidth(200);

        // Button variables
        Font buttonFont = Font.font("Courier New", FontWeight.BOLD, 16);
        int buttonMaxWidth = 125;

        // Button for dealing hand
        Button buttonDealHand = new Button("Deal hand");
        buttonDealHand.setMaxWidth(buttonMaxWidth);
        buttonDealHand.setFont(buttonFont);
            buttonDealHand.setOnAction(click -> {
                try{
                    hand = new HandOfCards(cards.dealHand(makeStringToInt(amountCards)));
                } catch (IndexOutOfBoundsException e){
                    Popup.showErrorPopup(e.getMessage());
                }
            });
        // Button for displaying your hand
        Button buttonShowHand = new Button("Show hand");
        buttonShowHand.setMaxWidth(buttonMaxWidth);
        buttonShowHand.setFont(buttonFont);
        buttonShowHand.setOnAction(click -> ShowHandGUI.display("Your hand", hand.getHand()));

        // Button for checking different features on your hand
        Button buttonCheckHand = new Button("Check hand");
        buttonCheckHand.setMaxWidth(buttonMaxWidth);
        buttonCheckHand.setFont(buttonFont);
        buttonCheckHand.setOnAction(click -> {
            String sumOfFaces = Integer.toString(hand.getSumOfTheFaces());
            sumOfFacesText.setText("Sum of the faces: " + sumOfFaces);

            ArrayList<PlayingCard> cardsOfHearts = hand.getCardsOfHearts();
            String hearts = cardsOfHearts.stream().map(PlayingCard::toString).collect(Collectors.joining(" ","",""));
            if (hearts.isEmpty()){
                hearts = "No hearts";
            }
            cardsofHeartsText.setText("Cards of hearts: " + hearts);

            String flush = "";
            if(hand.hasFlush()){
                flush = "Yes";
            }
            else {
                flush = "No";
            }
            flushText.setText("Flush: " + flush);

            String queenOfSpades ="";
            if(hand.hasQueenOfSpade()){
                queenOfSpades = "Yes";
            }
            else {
                queenOfSpades = "No";
            }
            queenOfSpadesText.setText("Queen of spades: " + queenOfSpades);
        });

        buttonLayout.getChildren().addAll(buttonDealHand, buttonShowHand, buttonCheckHand);
        attributeLayout.getChildren().addAll(new HBox(10, sumOfFacesText, cardsofHeartsText), new HBox(10, flushText, queenOfSpadesText));
        attributeLayout.setAlignment(Pos.BOTTOM_LEFT);

        stackPane.getChildren().addAll(attributeLayout, buttonLayout, amountCardsLayout);

        Scene scene = new Scene(stackPane, 500, 280, Color.DODGERBLUE);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method for making a String to Int
     * Used by the TextField for entering deck size.
     * Throws NumberFormatExcepetion if a number was not entered.
     * @param input   The input you want converted
     * @return   input as an int (if possible)
     */
    private int makeStringToInt(TextField input){
        try{
            int number = Integer.parseInt(input.getText());
            return number;

        }catch(NumberFormatException e){
            Popup.showErrorPopup(input.getText() + " is not a number");
            return 0;
        }
    }

    public static void main(String[] args) {
        launch();
    }

}
