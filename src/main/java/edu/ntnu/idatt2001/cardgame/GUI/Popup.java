package edu.ntnu.idatt2001.cardgame.GUI;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class is for displaying an error message
 * when the user inputs a wrong value for the amount
 * of cards they want.
 */
public class Popup{
    public static void showErrorPopup(String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(message);
        alert.showAndWait();
    }
}
