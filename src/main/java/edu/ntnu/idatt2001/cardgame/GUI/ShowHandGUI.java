package edu.ntnu.idatt2001.cardgame.GUI;

import edu.ntnu.idatt2001.cardgame.PlayingCard;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.util.ArrayList;

public class ShowHandGUI {
    /**
     * GUI for displaying your hand.
     * @param title   Title of the Stage
     * @param hand   The hand you want to display
     */
    public static void display(String title, ArrayList<PlayingCard> hand){

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        VBox layout = new VBox();

        Text yourHand = new Text("Your hand:");
        yourHand.setFont(Font.font("verdana", FontWeight.EXTRA_BOLD, FontPosture.REGULAR, 30));
        layout.getChildren().add(yourHand);

        Button buttonClose = new Button();
        buttonClose.setText("Close");
        buttonClose.setOnAction(click -> window.close());

        // For-loop for displaying every card as a png
        HBox layoutImage = new HBox();
        layoutImage.setAlignment(Pos.TOP_LEFT);
        int index = 0;
        for(PlayingCard card : hand){
            Image img = new Image(ShowHandGUI.class.getResource("/Cards/"+card.toString()+".png").toString());
            ImageView imageView = new ImageView(img);
            imageView.setFitHeight(150);
            imageView.setFitWidth(80);
            imageView.setPreserveRatio(false);
            layoutImage.getChildren().add(imageView);
            index+=1;
            if(index>11){
                layout.getChildren().addAll(new HBox(5,layoutImage.getChildren().get(0), layoutImage.getChildren().get(1), layoutImage.getChildren().get(2), layoutImage.getChildren().get(3), layoutImage.getChildren().get(4), layoutImage.getChildren().get(5),layoutImage.getChildren().get(6),layoutImage.getChildren().get(7), layoutImage.getChildren().get(8), layoutImage.getChildren().get(9), layoutImage.getChildren().get(10), layoutImage.getChildren().get(11)));
                layoutImage.getChildren().clear();
                index = 0;
            }
        }
        layout.getChildren().add(layoutImage);
        layout.getChildren().add(buttonClose);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
}
