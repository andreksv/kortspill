package edu.ntnu.idatt2001.cardgame;

import java.util.*;

/**
 * Represents a deck of PlayingCards.
 */
public class DeckOfCards {
    private final char[] suits = {'S', 'H', 'D', 'C'}; // 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private final int[] faces = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    private final List<PlayingCard> deck;

    /**
     * The class constructor.
     * Creates an ArrayList with every playing card (52 in total).
     */
    public DeckOfCards(){
        this.deck = new ArrayList<>();
        for(int face : faces){
            for(char suit : suits){
                this.deck.add(new PlayingCard(suit,face));
            }
        }
    }

    /**
     * Method for getting the Deck.
     * @return   An ArrayList of all the PlayingCards in the deck.
     */
    public List<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Returns an ArrayList with a specified amount of cards.
     * If n is less than 1 or more than 52, throws IndexOutofBoundsException.
     * @param n   The amount of cards you want to get
     * @return   The ArrayList with n playing cards.
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IndexOutOfBoundsException{
        if(n<1 || n>deck.size()){
            throw new IndexOutOfBoundsException("You can´t have a hand of less than 1 or more than 52 cards.");
        }
        ArrayList<PlayingCard> hand = new ArrayList<>();
        ArrayList<PlayingCard> copyDeck = new ArrayList(deck);
        Random random = new Random();

        for(int i =0; i<n;i++){
            int randomInteger = random.nextInt(copyDeck.size());
            hand.add(copyDeck.get(randomInteger));
            copyDeck.remove(randomInteger); //Removes the playing card added from the copy to avoid duplicates.
        }
        return hand;

    }
}